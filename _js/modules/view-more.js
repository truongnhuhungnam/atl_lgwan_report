/*
 *
 * ----------------------------------------------- */
jQuery(function ($) {
  var $viewmore = $("#view-more-1, #view-more-2");
  var $btn = ".btn-view-more";

  $viewmore.on("show.bs.collapse", function (e) {
    $(this).closest(".accordion").find($btn).text("Close");
  });

  $viewmore.on("hide.bs.collapse", function (e) {
    $(this).closest(".accordion").find($btn).text("View more");
  });
});
