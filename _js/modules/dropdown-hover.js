import { isMobile } from "./isMobile";

/*
 * collapse animation for navbar dropdown
 * ----------------------------------------------- */
jQuery(function ($) {
  if (isMobile()) {
    $('[data-toggle-touch="collapse"]').on("click", function (e) {
      $(this)
        .parent()
        .toggleClass("open-dropdown-xs")
        .children(".collapse")
        .collapse("toggle");

      e.preventDefault();
    });
  }

  $('[data-toggle-hover="collapse"]')
    .parent()
    .hover(
      function () {
        var $this = $(this);

        $this.addClass("open").children(".collapse").collapse("show");

        var timer = setInterval(function () {
          if (
            $this.hasClass("open") &&
            $this.children(".collapse-child").css("display") === "none"
          ) {
            $this.children(".collapse").collapse("show");
          }

          if ($this.children(".collapse").is(":visible")) {
            clearTimeout(timer);
          }
        }, 100);
      },
      function () {
        var $this = $(this);

        $this.removeClass("open");

        var timer = setInterval(function () {
          if (
            !$this.hasClass("open") &&
            $this.children(".collapse-child").css("display") === "block"
          ) {
            $this.children(".collapse").collapse("hide");
          }

          if ($this.children(".collapse").is(":hidden")) {
            clearTimeout(timer);
          }
        }, 100);
      }
    );
});
